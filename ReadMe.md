 
### ReadMe file for build a vagrant machine and delpoy Ansible Palybook ###

## 1- Create Vagrantfile / ansible.cfg / inventory
## 2- Lancer la machine vituel
        cmd: - vagrant up
        # config vagrant ssh
        - vagrant ssh-config
        # test ansible conection to vagrant vm
        - ansible $server -i inventory -m ping
        # launch tools-apt to install all packages
        - ansible-playbook Install-tools.yml --become -K
